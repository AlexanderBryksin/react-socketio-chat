import React, { Component } from 'react';
import axios from 'axios';
import { Table } from 'reactstrap';
import openSocket from 'socket.io-client';

// Get user data
// Connect to server
// emit user to server
// update table

// https://www.geoplugin.com/webservices/json
// http://socketserve.io/

const socket = openSocket('http://localhost:6600');

class LiveVisitors extends Component {
  state = {
    visitors: [
      // {
      //   ip: 'geoplugin_request',
      //   countryCode: 'geoplugin_countryCode',
      //   city: 'geoplugin_city',
      //   state: 'geoplugin_region',
      //   country: 'geoplugin_countryName'
      // }
    ]
  };

  async componentDidMount() {
    const { data } = await axios.get(`http://www.geoplugin.net/json.gp`);

    const {
      geoplugin_request,
      geoplugin_countryCode,
      geoplugin_city,
      geoplugin_region,
      geoplugin_countryName
    } = data;

    const visitor = {
      ip: geoplugin_request,
      countryCode: geoplugin_countryCode,
      city: geoplugin_city,
      state: geoplugin_region,
      country: geoplugin_countryName
    };

    console.log('visitior => ', visitor);
    socket.emit('new_visitor', visitor);

    socket.on('visitors', visitors => {
      console.log('visitors from socket:', visitors);
      this.setState({ visitors: [...this.state.visitors, visitor] });
    });
  }

  renderTableBody = () => {
    const { visitors } = this.state;
    console.log(`state visitors ${this.state.visitors}`);

    return visitors.map((v, index) => {
      console.log('from map', v);
      return (
        <tr key={index}>
          <td>{index}</td>
          <td>{v.ip}</td>
          <td>{v.country}</td>
          <td>{v.city}</td>
          <td>{v.state}</td>
          <td>{v.country}</td>
        </tr>
      );
    });
  };

  render() {
    return (
      <React.Fragment>
        <h2>Live Visitors</h2>
        <Table>
          <thead>
            <tr>
              <th>#</th>
              <th>Ip Adress</th>
              <th>Flag</th>
              <th>City</th>
              <th>State</th>
              <th>Country</th>
            </tr>
          </thead>
          <tbody>{this.renderTableBody()}</tbody>
        </Table>
      </React.Fragment>
    );
  }
}

export default LiveVisitors;
