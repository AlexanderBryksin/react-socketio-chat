const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);

const port = 6800;

app.get('/', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket) {
  console.log('a user connected: ', socket.id);

  socket.on('join_room', room => {
    socket.join(room);
  });

  socket.on('message', data => {
    // message and room in data
    const { room, message } = data;
    socket.to(room).emit('message', {
      message,
      name: 'Friend'
    });
  });

  socket.on('typing', data => {
    const { room } = data;
    socket.to(room).emit('typing', 'Someone is typing ...');
  });

  socket.on('stopped_typing', data => {
    const { room } = data;
    socket.to(room).emit('stopped_typing');
  });
});

http.listen(port, function() {
  console.log(`listening on *:${port}`);
});
